using Toybox.WatchUi as Ui;
using Toybox.System as Sys;
using Toybox.Lang;
using Toybox.Attention;

class PingpongDelegate extends Ui.BehaviorDelegate {
	
	var currentView;
	var currentGameState;

	function initialize(view, gameState) {
        BehaviorDelegate.initialize();
        currentView = view;
        currentGameState = gameState;
    }
    
	function onBack() {
		if (currentGameState.gameStarted == false && (currentGameState.goal1 == 0 || currentGameState.goal2 == 0)) {
			return true;
		}
	    currentGameState.undoGoal();
		checkWinOrPass();
		Ui.requestUpdate();
		if (currentGameState.gameStarted == false && (currentGameState.goal1 > 0 || currentGameState.goal2 > 0)) {
			currentGameState.gameStarted = true;
		}
		return true;
	}
    
    function onMenu() {
        var menu = new WatchUi.Menu();
        var delegate;
        menu.setTitle("My Menu");
        menu.addItem("Exit", :one);
        delegate = new MyMenuInputDelegate();
        WatchUi.pushView(menu, delegate, WatchUi.SLIDE_IMMEDIATE);
        return true;
    }

    function onTap1() {
		if (!currentGameState.gameStarted) {
			currentGameState.init();
			currentGameState.startedGamer = currentGameState.PLAYER_UP;
			checkOnBegin(currentGameState.PLAYER_UP);
			return;
		}
		currentGameState.incGoal1();
		checkWinOrPass();
    }

    function onTap2() {
		if (!currentGameState.gameStarted) {
			currentGameState.init();
			currentGameState.startedGamer = currentGameState.PLAYER_DOWN;
			checkOnBegin(currentGameState.PLAYER_DOWN);
			return;
		}
		currentGameState.incGoal2();
		checkWinOrPass();
    }
    
    function checkOnBegin(currentPlayer) {
    	if (currentGameState.gameStarted == false) {
			currentView.findDrawableById("menuTextLabel").setText("");
			currentView.findDrawableById("undoLastTurn").setText("");
    		currentGameState.gameStarted = true;
    		updateViews(currentPlayer);
      	}
    }
    
    function checkWinOrPass() { 
    	updateViews(currentGameState.whoHasBallNow()); 
    	var winPlayer = currentGameState.checkIfWin();
    	if (winPlayer >= 0) {
    		if (winPlayer == currentGameState.PLAYER_UP) {
				currentView.findDrawableById("label1").setText("WIN!");
			} else {
				currentView.findDrawableById("label2").setText("WIN!");
			}
			
			if (Attention has :vibrate) {
		    	var vibeData = [ 
        			new Attention.VibeProfile(50, 1000),
    			];
    			Attention.vibrate(vibeData);		
			}
			
			currentGameState.gameStarted = false;
    	}
    }
    
    function updateViews(currentPlayer) {
    	if (currentPlayer == currentGameState.PLAYER_UP) {
	    	currentView.findDrawableById("labelGoGoUp").setText("*");
	    	currentView.findDrawableById("labelGoGoBottom").setText("");
    	} else {
	    	currentView.findDrawableById("labelGoGoUp").setText("");
	    	currentView.findDrawableById("labelGoGoBottom").setText("*");
    	}    	
		currentView.findDrawableById("label1").setText(currentGameState.goal1.toString());
 		currentView.findDrawableById("label2").setText(currentGameState.goal2.toString());		
    }
}