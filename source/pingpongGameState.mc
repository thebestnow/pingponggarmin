class PingpongGameState {
	enum {
		PLAYER_UP = 0,
		PLAYER_DOWN
	}

	var goal1;
	var goal2;
	var gameStarted;
	var currentBallInGame;
	var startedGamer;
	var moreLess;
    var goalsHistory;
    var moreLessTurn;
	
	function initialize() {
		init();
	}
	
	function init() {
		goalsHistory = new [1000];
		goal1 = 0;
		goal2 = 0;
		gameStarted = false;
		currentBallInGame = -1;
		moreLess = false;
		startedGamer = PLAYER_UP;
		moreLessTurn = -1;
	}
	
	function checkIfWin() {	
		if (goal1 == 10 && goal2 == 10) {
			moreLess = true;
			moreLessTurn = currentBallInGame;
		}
		
		if (moreLess) {
			if (goal1 - goal2 == 2) {
				return PLAYER_UP;
			}
			
			if (goal2 - goal1 == 2) {
				return PLAYER_DOWN;
			}
			
			return -1;
		}
	
		if (goal1 == 11) {
			return PLAYER_UP;
		}
		if (goal2 == 11) {
			return PLAYER_DOWN;
		}
		
		return -1;
	}

	function undoGoal() 
	{
		if (currentBallInGame >= 0)
		{
			if (moreLess && currentBallInGame < moreLessTurn) {
				moreLess = false;
				moreLessTurn = -1;
			}
			
			var lastGamerBall = goalsHistory[currentBallInGame];			
			if (lastGamerBall == PLAYER_UP) {
				goal1 = goal1 - 1;
			} else if (lastGamerBall == PLAYER_DOWN) 	{
				goal2 = goal2 - 1;
			}
				
			currentBallInGame = currentBallInGame - 1;
		}		
	}

	function incGoal1() {
		goal1 = goal1 + 1;
		currentBallInGame = currentBallInGame + 1;
		goalsHistory[currentBallInGame] = PLAYER_UP;
	}

	function incGoal2() {
		goal2 = goal2 + 1;
		currentBallInGame = currentBallInGame + 1;
		goalsHistory[currentBallInGame] = PLAYER_DOWN;
	}
	
	function whoHasBallNow() {
		var whoHasBall = startedGamer;
	    if (currentBallInGame != -1) {
	        if (moreLess) {
	        	if ((currentBallInGame - moreLessTurn) % 2 == 0) {
		    		whoHasBall = startedGamer;	        	
	        	}
	        	else {
		    		whoHasBall = 1 - startedGamer;    			        	
	        	}
	        }
	        else 
		    {
		    	System.println(currentBallInGame);
		    
		        if ((currentBallInGame + 1) % 4 < 2 ) {
		    		whoHasBall = startedGamer;
		    	} else {
		    		whoHasBall = 1 - startedGamer;    		
		    	}
			}
    	}
    	return whoHasBall;
	}
}