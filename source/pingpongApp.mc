using Toybox.Application as App;
using Toybox.WatchUi as Ui;

class PingpongApp extends App.AppBase {
	var gameState;
	var view;
	
    function initialize() {
        AppBase.initialize();
        view = new PingpongView();
    	gameState = new PingpongGameState();
    }

    // onStart() is called on application start up
    function onStart(state) {
    }

    // onStop() is called when your application is exiting
    function onStop(state) {
    }

    // Return the initial view of your application here
    function getInitialView() {
    	return [ view, new PingpongDelegate(view, gameState) ];
    }
}
